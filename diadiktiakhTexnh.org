#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS: [a4paper,12pt]
#+OPTIONS: toc:nil
#+LANGUAGE: gr
#+LATEX_HEADER: \usepackage{fontspec}
#+LATEX_HEADER: \usepackage{libertine}
#+LATEX_HEADER: \usepackage[greek]{babel}
#+LATEX_HEADER: \usepackage[margin=0.75in]{geometry}
#+TITLE: inputNoise paper
#+AUTHOR: Νικόλαος Χαραλαμπίδης
#+STARTUP: showeverything inlineimages
#+BEGIN_abstract
περιοχή για abstract.
#+END_abstract
* Εισαγωγή
  Απο τα μέσα της δεκαετίας του 90' και με την ευρεία προσβασή του
  κοινού στο διαδίκτυο γεννήθηκε και η διαδυκτιακή τεχνη. Καλλιτέχνες
  άρχησαν την εξερεύνηση των ικανοτήτων και χαρακτηριστηκών του www. Σήμερα το
  διαδίκτυο έχει εισχωρήσει στην καθήμερινότητα. Το τεράστιο ποσό
  πληροφορίας που διαχέει την ψηφιακή ρουτίνα με την μορφή κειμένου,
  εικόνας και ήχου. Σε αυτή την εργασία θα εξετασθεί η σχέση
  διαδικτυακής τέχνης και generative art {και πως δημιουγήθηκε το Post
  Internet κίνημα}

* Internet Art
  ειδοι Internet art
cite:johnson13_artis_music_applic_inter_searc_techn
* generative art
cite:doi:10.1080/14626260902867915
* Computer music

* Post-internet art
???
* inputNoise
  Σε αυτή την ενότητα παρουσιάζεται η εργασία/έργο διαδικτυακής τέχνης
του συγγραφέα με ονομασία /inputNoise/. Το έργο περιγράφεται ως μη
διαδραστική διαδικτυακή τέχνη με χαρακτηριστικά G-Art
cite:doi:10.1080/14626260902867915.

** Περιγραφή
   Το έργο αποτελείτε απο 2 βασικά στάδια. Στο πρώτο στάδιο βρίσκονται
   τυχαία δείγματα ήχου απο το διαδίκτυο και τα ετοιμάζει προς χρήση
   για το δεύτερο στάδιο. Στο δεύτερο στάδιο χρησιμοποιήτε το δείγμα
   ήχου για την παραγωγή ήχων κυρίος μέσο τυχαίων διαδικασίων και
   granular synthesis cite:roads_microsound_2001.
*** Σταδιο 1ο
    Το πρώτο στάδιο έχει αναπτυχθεί σε γλώσσα python.  Αυτό το στάδιο
    αποτελείτε απο ένα πρόγραμμα/script το οποίο βασίζεται στο
    _YOUTUBE API_[WHAT IS AN API]. Μέσω του _YOUTUBE API_ επίλεγεται
    ένα τυχαίο βίντεο άπο τον κατάλογο του τις ιστιοσελίδας
    youtube.com. Από αυτό κατεβάζεται(download) ο ήχος του πρώτου
    λέπτου εαν είναι διάρκειας 1ος λέπτου και άνω. Αυτή η διαδικάσια
    επαναλαμβάνεται κάθε 25λεπτα λόγο περιορισμών του _YOUTUBE
    API_.
*** Σταδιο 2ο (Supercollider)
    Εδώ το δείγμα ήχου επεξεργάζεται χρησιμοποιώντας την μέθοδο της
granular synthesis cite:roads_microsound_2001. Παράμετροι τις
σύνθεσης, οπως η διάρκεια του grain κ.α., διαχειρίζονται στοχαστίκα.
*** Docker container
τι είναι το docker και container.

bibliographystyle:apalike
bibliography:bib/biblio.bib,bib/MyLibrary.bib
